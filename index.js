var express = require('express'); // ExpressJS for Web Server
var socketio = require('socket.io'); // Socket.io for Server-Client connections
var hbs = require('handlebars'); // Handlebars for Page Rendering.. Am I gonnna use this?
var helpers = require('handlebars-helpers')({ // Helpers for Handlebars
	handlebars: hbs
});
var rp = require('request-promise'); // CURL Requests
var Datastore = require('nedb-promise'); // Database
var http = require('http'); // HyperText Transfer Protocol
var config = require('./config.json'); // Configuration
var fs = require('fs'); // File System
var bodyParser = require('body-parser'); // POST Body
var cookieParser = require('cookie-parser'); // Nom.. nom..
var page;
var logclan = true;

var db = {}; // Database Object
db.access = new Datastore('db/access.db'); // Sets up Access DB
db.access.loadDatabase(); // Loads Access DB
db.cl = new Datastore('db/clanlist.db'); // Sets up ClanList DB
db.cl.loadDatabase(); // Loads ClanList DB
db.cf = new Datastore('db/clanfeed.db'); // Sets up ClanFeed DB
db.cf.loadDatabase(); // Loads ClanFeed DB
db.stats = new Datastore('db/stats.db'); // Sets up Stats DB
db.stats.loadDatabase(); // Loads Stats DB

var app = express(); // Setup Express Application
app.use(express.static('public')); // Public Files
app.use(bodyParser.json()); // POST Requests
app.use(bodyParser.urlencoded({ extended: true })); // POST Requests
app.use(cookieParser()); // Cookies


var timezone,hqT,doffset = 0;
(async function(){
	timezone = JSON.parse(await rp('https://api.timezonedb.com/v2/get-time-zone?key=S0T3SKWNPU73&format=json&by=position&lat=33.214561&lng=-96.614456'));
	doffset = (timezone.gmtOffset * 1000) + ((new Date()).getTimezoneOffset() * 60000);
})();
hqT = function(){return new Date(Date.now() + doffset);};

var translations = {
	"en_gb": ["fr","en_us","pirate"],
	"Requirement": ["Besoin","Requirement","Captain's Orders"],
	"Requirements": ["Besoins","Requirements","Captain's Orders"],
	"Authenticate": ["Authentifier","Authenticate","Log ye in"],
	"Create clan shortcut": ["Créer un raccourci de clan","Create Clan Shortcut","Create Fleet Shortcut"],
	"is a registered trademark of": ["est une marque déposée de","is a registered trademark of","be a register'd trademark o'"],
	"Player": ["Joueur","Player", "Pirate"],
	"Level": ["Niveau","Level","Level"],
	"Ranking": ["Classement","Ranking","Rankin'"],
	"Members": ["Membres","Members", "Crew"],
	"For this month": ["Pour ce mois","For this month"],
	"Add a Clan": ["Ajouter un clan","Add a Clan","Add a Ship"],
	"Add": ["Ajouter","Add", "Add"]
}

var requirements = [null,null,null,null,
	{req: true,clan: [
		{tot_p:85000,cap:200},
		{tot_p:170000,cap:350},
		{tot_p:400000,cap:1000,gam_cap:10},
		{tot_p:600000,cap:2500,gam_cap:25,phy_dep:100},
		{tot_p:850000,cap:5000,gam_cap:75,phy_dep:250}
	],user: [
		{tot_p:6500},
		{tot_p:17500,cap:30},
		{tot_p:35000,cap:75},
		{tot_p:50000,cap:150},
		{tot_p:75000,cap:300}
	]},
	{"req":true,"clan":[
		{"phy_cap":250,"phy_dep":150,"tot_p":100000},
		{"phy_cap":500,"phy_dep":300,"tot_p":250000},
		{"phy_cap":800,"phy_dep":400,"tot_p":500000},
		{"phy_cap":1100,"phy_dep":700,"tot_p":999999},
		{"phy_cap":2000,"phy_dep":1000,"myth_cap":50,"tot_p":1500000}
	],"user":[
		{"tot_p":6000},
		{"tot_p":15000,"phy_cap":20},
		{"tot_p":35000,"phy_cap":50,"phy_dep":25},
		{"tot_p":50000,"phy_cap":70,"phy_dep":50},
		{"tot_p":100000,"phy_cap":100,"phy_dep":80}
	]},
	{"req":true,"clan":[
		{"c_days":7,"tot_p":100000},
		{"c_days":10,"d_days":7,"tot_p":225000},
		{"c_days":14,"d_days":14,"clan_dep":100,"tot_p":450000},
		{"c_days":21,"d_days":21,"clan_dep":175,"jewel_dep":100,"tot_p":700000},
		{"c_days":28,"d_days":28,"clan_dep":200,"jewel_dep":200,"tot_p":1000000}
	],"user":[
		{"c_days":7,"tot_p":5000},
		{"c_days":10,"d_days":7,"tot_p":12000},
		{"c_days":14,"d_days":14,"tot_p":30000},
		{"c_days":21,"d_days":21,"tot_p":50000},
		{"c_days":28,"d_days":28,"tot_p":85000}
	]}
];



async function getUName(id, doc){ // Get Username of User
	var auth = doc ? doc.token.access_token : await getAT();
	var options = {
		method: 'POST',
		uri: 'https://api.munzee.com/user/',
		headers: {
			'Authorization': auth, 
		},
		formData: {
			data: `{"user_id":${id}}`, 
		}
	};
	var body = await rp(options);
	var data = JSON.parse(body).data;
	return data.username;
}

async function getAT(id, expired){ //Get User Access Token
    var doc;
    if(id && typeof id == "number"){
        doc = await db.access.findOne({user_id: id});
    } else if(id){
        doc = await db.access.findOne({username: id});
    } else {
        doc = await db.access.findOne({});
	}
	if(!doc) return false;
	var newdoc = doc;
    if((Date.now() + 60000) / 1000 > newdoc.token.expires || expired){
		try {
			var options = {
				method: 'POST',
				uri: 'https://api.munzee.com/oauth/login',
				formData: {
					'client_id': config.client_id,
					'client_secret': config.client_secret,
					'grant_type': 'refresh_token',
					'refresh_token': doc.token.refresh_token, 
					'redirect_uri': config.redirect_uri
				}
			};
			Object.assign(newdoc, JSON.parse(await rp(options)).data);
			newdoc.username = await getUName(newdoc.user_id, newdoc);
			await db.access.update(doc,newdoc);
		} catch(e) {
			console.log(doc.username);
			if(e.toString().includes('The refresh token is invalid.')){
				await db.access.remove({"token.refresh_token": doc.token.refresh_token});
			}
		}
    }
    return newdoc.token.access_token;
}

async function mR(page, data, id, expired){ // Munzee Request
	var auth = id ? id : await getAT();
	if(!auth) console.log('No auth?');
	var options = {
		method: 'POST',
		uri: encodeURI('https://api.munzee.com/' + page),
		headers: {
			Authorization: encodeURI(auth),
		},
		formData: {
			data: JSON.stringify(data), 
		}
	};
	// console.log("OPTIONS:", options);
	try {
		var raw = await rp(options)
		if(raw.includes('The refresh token is invalid.')){
			await db.access.remove({"token.access_token": auth});
			return await mR(page, data, await getAT(uid), true);
		}
		var body = JSON.parse(raw).data;
		return body;
	} catch(e) {
		// console.log('Unexpected error occurred', e);
		var uid = await db.access.findOne({"token.access_token": auth});
		if(e.toString().includes('The refresh token is invalid.')){
			await db.access.remove({"token.access_token": auth});
			expired = false;
		}
		if(!expired) {
			return await mR(page, data, await getAT(uid), true);
		} else {
			await db.access.remove({"token.access_token": auth});
		}
	}
	
}

var clanlist = [],userlist = [];
var captureids = [];

async function getClanData(clanid){
	console.log('Reloading Clan' + clanid);
	//var munzees = JSON.parse(await rp('https://gitlab.com/MOBlox/munzee-list/raw/master/munzee-list.json'));
	var munzees;
	if(!munzees || !Object.keys(munzees).length || Object.keys(munzees).length < 10) {
		munzees = {"0":{"Name":"Greenie","physical":true,"color":"green","id":"normal","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/munzee.svg"},"1":{"Name":"Mystery","physical":true,"old":true,"color":"blue","mystery":true,"id":"mystery","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mystery.svg"},"2":{"Name":"Business","physical":true,"old":true,"color":"purple","id":"business","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/business.svg"},"3":{"Name":"Virtual","virtual":true,"color":"white","id":"virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual.svg"},"4":{"Name":"Premium","physical":true,"color":"orange","premium":true,"id":"premium","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/premium.svg"},"5":{"Name":"Mystery","physical":true,"old":true,"color":"blue","mystery":true,"id":"mystery","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mystery.svg"},"6":{"Name":"NFC","nfc":true,"physical":true,"color":"lightblue","id":"nfc","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/nfc.svg"},"10":{"Name":"Virtual","virtual":true,"color":"white","id":"virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual.svg"},"13":{"Name":"Premium","physical":true,"color":"orange","premium":true,"id":"premium","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/premium.svg"},"22":{"Name":"Business","physical":true,"old":true,"color":"purple","id":"business","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/business.svg"},"32":{"Name":"Social","social":true,"locationless":true,"color":"green","id":"social","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/social.svg"},"40":{"Name":"Diamond","physical":true,"jewel":true,"split":true,"color":"white","id":"diamond","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/diamond.svg"},"52":{"Name":"Mace","physical":true,"clan":true,"split":true,"color":"grey","id":"mace","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mace.svg"},"53":{"Name":"Longsword","physical":true,"clan":true,"split":true,"color":"grey","id":"longsword","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/longsword.svg"},"70":{"Name":"Motel","physical":true,"hasrooms":true,"color":"lightblue","id":"motel","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/motel.svg"},"71":{"Name":"Motel Room","physical":true,"room":true,"color":"lightblue","id":"motel-room","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/motel.svg"},"73":{"Name":"Quiz Virtual","virtual":true,"quiz":true,"color":"white","id":"quiz-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/quizvirtual.svg"},"76":{"Name":"Moustache","virtual":true,"old":true,"temporary":true,"color":"white","id":"moustache","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/movember.svg"},"80":{"Name":"Mystery Virtual","virtual":true,"mystery":true,"old":true,"color":"blue","id":"mystery-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mysteryvirtual.svg"},"103":{"Name":"Red Virtual","virtual":true,"color":"red","id":"red-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_red.svg"},"104":{"Name":"Orange Virtual","virtual":true,"color":"orange","id":"orange-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_orange.svg"},"105":{"Name":"Yellow Virtual","virtual":true,"color":"yellow","id":"yellow-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_yellow.svg"},"106":{"Name":"Green Virtual","virtual":true,"color":"green","id":"green-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_green.svg"},"107":{"Name":"Blue Virtual","virtual":true,"color":"lightblue","id":"blue-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_blue.svg"},"108":{"Name":"Indigo Virtual","virtual":true,"color":"blue","id":"indigo-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_indigo.svg"},"109":{"Name":"Violet Virtual","virtual":true,"color":"purple","id":"violet-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_violet.svg"},"110":{"Name":"Black Virtual","virtual":true,"color":"black","id":"black-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_black.svg"},"111":{"Name":"Brown Virtual","virtual":true,"color":"brown","id":"brown-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_brown.svg"},"112":{"Name":"Rainbow Virtual","virtual":true,"color":"rainbow","id":"rainbow-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_rainbow.svg"},"113":{"Name":"Red Mystery Virtual","virtual":true,"mystery":true,"color":"red","id":"red-mystery-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_mystery_red.svg"},"114":{"Name":"Orange Mystery Virtual","virtual":true,"mystery":true,"color":"orange","id":"orange-mystery-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_mystery_orange.svg"},"115":{"Name":"Yellow Mystery Virtual","virtual":true,"mystery":true,"color":"yellow","id":"yellow-mystery-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_mystery_yellow.svg"},"116":{"Name":"Green Mystery Virtual","virtual":true,"mystery":true,"color":"green","id":"green-mystery-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_mystery_green.svg"},"117":{"Name":"Blue Mystery Virtual","virtual":true,"mystery":true,"color":"blue","id":"blue-mystery-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mysteryvirtual.svg"},"118":{"Name":"Indigo Mystery Virtual","virtual":true,"mystery":true,"color":"lightblue","id":"indigo-mystery-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_mystery_indigo.svg"},"119":{"Name":"Violet Mystery Virtual","virtual":true,"mystery":true,"color":"purple","id":"violet-mystery-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_mystery_violet.svg"},"120":{"Name":"Black Mystery Virtual","virtual":true,"mystery":true,"color":"black","id":"black-mystery-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_mystery_black.svg"},"121":{"Name":"Brown Mystery Virtual","virtual":true,"mystery":true,"color":"brown","id":"brown-mystery-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_mystery_brown.svg"},"122":{"Name":"Rainbow Mystery Virtual","virtual":true,"mystery":true,"color":"rainbow","id":"rainbow-mystery-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_mystery_rainbow.svg"},"123":{"Name":"RMH Virtual","virtual":true,"old":true,"temporary":true,"color":"white","id":"rmh-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/rmh_virtual.svg"},"131":{"Name":"Ruby","jewel":true,"physical":true,"split":true,"color":"red","id":"ruby","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/ruby.svg"},"140":{"Name":"Battle Axe","clan":true,"physical":true,"color":"grey","id":"battle-axe","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/battleaxe.svg"},"146":{"Name":"Ice Bucket","virtual":true,"temporary":true,"old":true,"color":"lightblue","id":"als","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/ice_bucket.svg"},"148":{"Name":"Emerald","virtual":true,"garden":true,"jewel":true,"color":"green","id":"emerald","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_emerald.svg"},"170":{"Name":"Hotel","physical":true,"hasrooms":true,"color":"black","id":"hotel","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/hotel.svg"},"171":{"Name":"Hotel Room","physical":true,"room":true,"color":"black","id":"hotel-room","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/hotel.svg"},"190":{"Name":"Trail","trail":true,"physical":true,"color":"green","id":"trail","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/trail.svg"},"196":{"Name":"2015 Yellow Heart","temporary":true,"virtual":true,"old":true,"color":"yellow","id":"15-heart-yellow","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/2015heartyellow.svg"},"197":{"Name":"2015 Green Heart","temporary":true,"virtual":true,"old":true,"color":"green","id":"15-heart-green","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/2015heartgreen.svg"},"198":{"Name":"2015 Orange Heart","temporary":true,"virtual":true,"old":true,"color":"orange","id":"15-heart-orange","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/2015heartorange.svg"},"199":{"Name":"2015 Pink Heart","temporary":true,"virtual":true,"old":true,"color":"pink","id":"15-heart-pink","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/2015heartpink.svg"},"200":{"Name":"Personal","social":true,"color":"orange","id":"personal","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/personalmunzee.svg"},"218":{"Name":"Aquamarine","physical":true,"jewel":true,"color":"lightblue","id":"aquamarine","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/aquamarine.svg"},"220":{"Name":"Sillyman","land":true,"bouncing":{"time":3,"on":[0]},"old":true,"temporary":true,"color":"lightblue","id":"sillyman","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/sillyman.svg"},"242":{"Name":"Topaz","physical":true,"jewel":true,"munzpak":true,"split":true,"color":"yellow","id":"topaz","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/topaz.svg"},"250":{"Name":"Accessibility","physical":true,"infinite":true,"accessible":true,"color":"lightblue","id":"access","pin":" https://munzee.global.ssl.fastly.net/images/pins/svg/accessibility.svg"},"280":{"Name":"Mini Mystery","physical":true,"mystery":true,"split":true,"color":"blue","id":"mini-mystery","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mystery.svg"},"290":{"Name":"Amethyst","virtual":true,"jewel":true,"color":"purple","id":"amethyst","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_amethyst.svg"},"294":{"Name":"Event Indicator","event":true,"color":"pink","id":"event-indicator","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/eventindicator.svg"},"297":{"Name":"Virtual Nomad","land":true,"nomad":true,"bouncing":{"time":12,"on":["virtual"]},"color":"blue","id":"virtual-nomad","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/nomadvirtual.svg"},"306":{"Name":"The Hammer","clan":true,"physical":true,"color":"grey","id":"hammer","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/thehammer.svg"},"307":{"Name":"Pink Mystery Virtual","virtual":true,"color":"pink","id":"pink-mystery-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_mystery_pink.svg"},"308":{"Name":"Scorpio","zodiac":true,"wzodiac":true,"physical":true,"color":"red","id":"scorpio","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/scorpio.svg"},"312":{"Name":"Sagittarius","zodiac":true,"wzodiac":true,"physical":true,"color":"lightblue","id":"sagittarius","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/sagittarius.svg"},"313":{"Name":"Capricorn","zodiac":true,"wzodiac":true,"physical":true,"color":"blue","id":"capricorn","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/capricorn.svg"},"314":{"Name":"Aquarius","zodiac":true,"wzodiac":true,"physical":true,"color":"lightblue","id":"aquarius","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/aquarius.svg"},"315":{"Name":"Pisces","zodiac":true,"wzodiac":true,"physical":true,"color":"lightblue","id":"pisces","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/pisces.svg"},"316":{"Name":"Aries","zodiac":true,"wzodiac":true,"physical":true,"color":"red","id":"aries","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/aries.svg"},"317":{"Name":"Taurus","zodiac":true,"wzodiac":true,"physical":true,"color":"orange","id":"taurus","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/taurus.svg"},"318":{"Name":"Gemini","zodiac":true,"wzodiac":true,"physical":true,"color":"pink","id":"gemini","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/gemini.svg"},"319":{"Name":"Cancer","zodiac":true,"wzodiac":true,"physical":true,"color":"yellow","id":"cancer","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/cancer.svg"},"320":{"Name":"Leo","zodiac":true,"wzodiac":true,"physical":true,"color":"yellow","id":"leo","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/leo.svg"},"321":{"Name":"Virgo","zodiac":true,"wzodiac":true,"physical":true,"color":"green","id":"virgo","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virgo.svg"},"322":{"Name":"Libra","zodiac":true,"wzodiac":true,"physical":true,"color":"green","id":"libra","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/libra.svg"},"353":{"Name":"Flat Rob","flat":true,"physical":true,"color":"white","id":"flat-rob","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/flatrob.svg"},"390":{"Name":"Virtual Trail","trail":true,"virtual":true,"color":"green","id":"virtual-trail","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_trail.svg"},"400":{"Name":"Surprise","virtual":true,"color":"blue","id":"surprise","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/surprise.svg"},"440":{"Name":"Virtual Shamrock","temporary":true,"virtual":true,"color":"green","id":"virtual-shamrock","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_shamrock.svg"},"441":{"Name":"Shamrock","physical":true,"color":"green","id":"shamrock","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/shamrock.svg"},"444":{"Name":"Prize Wheel","gaming":true,"physical":true,"color":"green","id":"prize-wheel","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/prizewheel.svg"},"470":{"Name":"Virtual Resort","hasrooms":true,"localdeploy":true,"virtual":true,"color":"yellow","id":"virtual-resort","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_resort.svg"},"471":{"Name":"Virtual Resort Room","room":true,"virtual":true,"color":"yellow","id":"virtual-resort-room","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtual_resort_room.svg"},"500":{"Name":"Scatter","scatter":true,"physical":true,"color":"green","id":"scatter","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/scatter.svg"},"501":{"Name":"Scattered","scattered":true,"land":true,"color":"white","id":"scattered","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/scattered.svg"},"505":{"Name":"Unicorn","myth":true,"bouncing":{"time":12,"on":[0,441]},"land":true,"color":"white","id":"unicorn","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/theunicorn.svg"},"506":{"Name":"Unicorn Host","mythhost":true,"physical":true,"color":"green","id":"unicorn-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/unicornhost.svg"},"508":{"Name":"Leprechaun","myth":true,"bouncing":{"time":12,"on":[0,441]},"land":true,"color":"yellow","id":"leprechaun","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/leprechaun.svg"},"509":{"Name":"Leprechaun Host","mythhost":true,"physical":true,"color":"green","id":"leprechaun-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/leprechaunhost.svg"},"510":{"Name":"Munzee Madness Reseller","reseller":"Munzee Madness","physical":true,"color":"green","id":"mm-rum","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/munzeemadnessreseller.svg"},"511":{"Name":"GeoStuff Reseller","reseller":"GeoStuff","physical":true,"color":"green","id":"geostuff-rum","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/geostuffreseller.svg"},"512":{"Name":"GeoHobbies Reseller","reseller":"GeoHobbies","physical":true,"color":"green","id":"geohobbies-rum","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/geohobbiesreseller.svg"},"513":{"Name":"DDCards Reseller","reseller":"DDCards","physical":true,"color":"green","id":"ddcards-rum","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/ddcardsreseller.svg"},"514":{"Name":"NEGS Reseller","reseller":"NEGS","physical":true,"color":"green","id":"negs-rum","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/negsreseller.svg"},"515":{"Name":"GeoLoggers Reseller","reseller":"GeoLoggers","physical":true,"color":"green","id":"geologgers-rum","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/geologgersreseller.svg"},"516":{"Name":"MM Cocoa Beach Reseller","reseller":"MM Cocoa Beach","physical":true,"color":"green","id":"mmcb-rum","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mmcocoabeachreseller.svg"},"521":{"Name":"Mystery Nomad","mystery":true,"land":true,"nomad":true,"bouncing":{"time":12,"on":[1,280]},"color":"blue","id":"mystery-nomad","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/nomadmystery.svg"},"522":{"Name":"Rock-Paper-Scissors","gaming":true,"physical":true,"color":"yellow","id":"rps","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/rockpaperscissors.svg"},"532":{"Name":"Fire Mystery","physical":true,"mystery":true,"color":"red","id":"fire-mystery","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/firemystery.svg"},"536":{"Name":"White MVM","virtual":true,"mystery":true,"split":true,"color":"white","id":"white-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmwhite.svg"},"537":{"Name":"Black MVM","virtual":true,"mystery":true,"split":true,"color":"black","id":"black-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmblack.svg"},"538":{"Name":"Blue MVM","virtual":true,"mystery":true,"split":true,"color":"blue","id":"blue-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmblue.svg"},"539":{"Name":"Brown MVM","virtual":true,"mystery":true,"split":true,"color":"brown","id":"brown-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmbrown.svg"},"540":{"Name":"Dark Green MVM","virtual":true,"mystery":true,"split":true,"color":"green","id":"darkgreen-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmdarkgreen.svg"},"541":{"Name":"Green MVM","virtual":true,"mystery":true,"split":true,"color":"green","id":"green-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmgreen.svg"},"542":{"Name":"Indigo MVM","virtual":true,"mystery":true,"split":true,"color":"blue","id":"indigo-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmindigo.svg"},"543":{"Name":"Light Blue MVM","virtual":true,"mystery":true,"split":true,"color":"lightblue","id":"lightblue-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmlightblue.svg"},"544":{"Name":"Orange MVM","virtual":true,"mystery":true,"split":true,"color":"orange","id":"orange-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmorange.svg"},"545":{"Name":"Pink MVM","virtual":true,"mystery":true,"split":true,"color":"pink","id":"pink-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmpink.svg"},"546":{"Name":"Purple MVM","virtual":true,"mystery":true,"split":true,"color":"purple","id":"purple-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmpurple.svg"},"547":{"Name":"Rainbow MVM","virtual":true,"mystery":true,"split":true,"color":"rainbow","id":"rainbow-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmrainbow.svg"},"548":{"Name":"Red MVM","virtual":true,"mystery":true,"split":true,"color":"red","id":"red-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmred.svg"},"549":{"Name":"Yellow MVM","virtual":true,"mystery":true,"split":true,"color":"yellow","id":"yellow-mvm","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mvmyellow.svg"},"550":{"Name":"Ruja Reseller","reseller":"Ruja","physical":true,"color":"green","id":"ruja-rum","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/rujareseller.svg"},"560":{"Name":"Fire","scattered":true,"land":true,"color":"red","id":"fire-spread","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/fire.svg"},"571":{"Name":"Recycle","land":true,"color":"green","id":"recycle","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mapcleanup.svg"},"573":{"Name":"Dragon","myth":true,"land":true,"bouncing":{"time":12,"on":[0,532]},"color":"red","id":"dragon","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/dragon.svg"},"574":{"Name":"Dragon Host","mythhost":true,"physical":true,"color":"green","id":"dragon-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/dragonhost.svg"},"584":{"Name":"Pink Diamond","jewel":true,"physical":true,"color":"pink","id":"pink-diamond","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/pinkdiamond.svg"},"600":{"Name":"Event Trail","event":true,"virtual":true,"trail":true,"color":"green","id":"event-trail","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/eventtrail.svg"},"601":{"Name":"Laupe Reseller","reseller":"Laupe","physical":true,"color":"green","id":"laupe-rum","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/laupereseller.svg"},"651":{"Name":"Ice Mystery","mystery":true,"scatter":true,"physical":true,"color":"lightblue","id":"ice-mystery","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/icemystery.svg"},"658":{"Name":"Tomato","evolution":"Farm","evstage":3,"physical":true,"color":"red","id":"tomato","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/tomato.svg"},"659":{"Name":"Ear of Corn","evolution":"Farm","evstage":3,"physical":true,"color":"yellow","id":"corn","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/earofcorn.svg"},"660":{"Name":"Carrot","evolution":"Farm","evstage":3,"virtual":true,"color":"orange","id":"carrot","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/carrot.svg"},"661":{"Name":"Peas","evolution":"Farm","evstage":3,"virtual":true,"color":"green","id":"peas","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/peas.svg"},"668":{"Name":"Carrot Plant","evolution":"Farm","evstage":2,"virtual":true,"color":"green","id":"carrot-plant","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/carrotplant.svg"},"678":{"Name":"Golden Carrot","evolution":"Farm","evstage":3,"premium":true,"physical":true,"color":"yellow","id":"golden-carrot","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/goldencarrot.svg"},"681":{"Name":"Sapphire","jewel":true,"virtual":true,"color":"blue","id":"sapphire","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/virtualsapphire.svg"},"682":{"Name":"TX Historical Location","garden":true,"virtual":true,"color":"black","id":"tx-history","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/txhistoricallocation.svg"},"684":{"Name":"Milk","evolution":"Farm","evstage":3,"physical":true,"color":"white","id":"milk","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/milk.svg"},"685":{"Name":"Bacon","evolution":"Farm","evstage":3,"physical":true,"color":"brown","id":"bacon","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/bacon.svg"},"686":{"Name":"BCA Garden","garden":true,"color":"pink","virtual":true,"id":"bca-garden","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/bcagarden.svg"},"687":{"Name":"BCA Pin","old":true,"virtual":true,"color":"pink","id":"bca-pin","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/bcapin.svg"},"696":{"Name":"SCGS Reseller","reseller":"SCGS","physical":true,"color":"green","id":"scgs-rum","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/scgsreseller.svg"},"697":{"Name":"Frozen Greenie","land":true,"scattered":true,"color":"lightblue","id":"frozen-greenie","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/frozengreenie.svg"},"698":{"Name":"Calf","evolution":"Farm","evstage":1,"physical":true,"color":"white","id":"calf","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/calf.svg"},"702":{"Name":"Championship Horse","evstage":3,"evolution":"Farm","virtual":true,"color":"brown","id":"horse","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/championshiphorse.svg"},"703":{"Name":"Eggs","evolution":"Farm","evstage":3,"virtual":true,"color":"white","id":"eggs","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/eggs.svg"},"707":{"Name":"Chicken","evolution":"Farm","evstage":2,"virtual":true,"color":"yellow","id":"chicken","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/chicken.svg"},"708":{"Name":"Barn","evolution":"Farm","evstage":3,"physical":true,"color":"red","id":"barn","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/barn.svg"},"709":{"Name":"Tractor","evolution":"Farm","evstage":3,"physical":true,"color":"green","id":"tractor","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/tractor.svg"},"725":{"Name":"Yeti","myth":true,"land":true,"bouncing":{"time":12,"on":[0,651]},"color":"white","id":"yeti","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/yeti.svg"},"726":{"Name":"Yeti Host","mythhost":true,"physical":true,"color":"white","id":"yeti-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/yetihost.svg"},"732":{"Name":"Plow","evolution":"Farm","evstage":1,"physical":true,"id":"plow","color":"brown","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/plow.svg"},"735":{"Name":"Family","evolution":"Farm","evstage":3,"virtual":true,"color":"blue","id":"family","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/family.svg"},"736":{"Name":"Field","evolution":"Farm","evstage":3,"virtual":true,"color":"green","id":"field","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/field.svg"},"738":{"Name":"Farmer and Wife","evolution":"Farm","evstage":2,"virtual":true,"color":"blue","id":"farmer-and-wife","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/farmerandwife.svg"},"741":{"Name":"Shark","evolution":"Education","evstage":3,"physical":true,"color":"blue","id":"shark","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/shark.svg"},"744":{"Name":"Submarine","evolution":"Education","evstage":3,"virtual":true,"color":"yellow","id":"submarine","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/submarine.svg"},"746":{"Name":"Motor Boat","evolution":"Education","evstage":2,"virtual":true,"color":"grey","id":"motorboat","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/motorboat.svg"},"782":{"Name":"POI Airport","poi":true,"virtual":true,"color":"orange","id":"poi-airport","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/poiairport.svg"},"783":{"Name":"POI Sports","poi":true,"virtual":true,"color":"orange","id":"poi-sports","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/poisports.svg"},"784":{"Name":"POI University","poi":true,"virtual":true,"color":"orange","id":"poi-university","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/poiuniversity.svg"},"786":{"Name":"POI Museum","poi":true,"virtual":true,"color":"orange","id":"poi-museum","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/poimuseum.svg"},"787":{"Name":"POI Wildlife","poi":true,"virtual":true,"color":"orange","id":"poi-wildlife","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/poiwildlife.svg"},"802":{"Name":"Bones","evolution":"Education","evstage":3,"physical":true,"color":"brown","id":"dinosaur","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/bones.svg"},"807":{"Name":"Muscle Car","evolution":"Education","evstage":3,"virtual":true,"color":"red","id":"muscle-car","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/musclecar.svg"},"809":{"Name":"Penny Farthing Bike","evolution":"Education","evstage":2,"virtual":true,"color":"red","id":"penny-farthing","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/penny-farthingbike.svg"},"835":{"Name":"King of the Jungle","evolution":"Education","evstage":3,"physical":true,"color":"orange","id":"lion","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/lion.svg"},"839":{"Name":"Safari Bus","evolution":"Education","evstage":3,"virtual":true,"color":"brown","id":"safari-bus","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/safaribus.svg"},"841":{"Name":"Safari Van","evolution":"Education","evstage":2,"virtual":true,"color":"brown","id":"safari-van","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/safarivan.svg"},"851":{"Name":"Earth Mystery","mystery":true,"scatter":true,"physical":true,"color":"green","id":"earth-mystery","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/earthmystery.svg"},"853":{"Name":"Faun","myth":true,"land":true,"bouncing":{"time":12,"on":[0,851]},"color":"brown","id":"faun","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/faun.svg"},"854":{"Name":"Faun Host","mythhost":true,"physical":true,"color":"green","id":"faun-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/faunhost.svg"},"862":{"Name":"Paw Garden","garden":true,"virtual":true,"color":"yellow","id":"paw-garden","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/pawgarden.svg"},"953":{"Name":"Hydra","myth":true,"land":true,"bouncing":{"time":12,"on":[0,1020]},"color":"purple","id":"hydra","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/hydra.svg"},"954":{"Name":"Hydra Host","mythhost":true,"physical":true,"color":"green","id":"hydra-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/hydrahost.svg"},"970":{"Name":"Timeshare","physical":true,"hasrooms":true,"color":"brown","id":"timeshare","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/time_share.svg"},"971":{"Name":"Timeshare Room","physical":true,"room":true,"color":"brown","id":"timeshare-room","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/time_share.svg"},"998":{"Name":"Crossbow","clan":true,"virtual":true,"color":"brown","id":"crossbow","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/crossbow.svg"},"999":{"Name":"Warrior Nomad","clan":true,"land":true,"nomad":true,"bouncing":{"time":12,"on":["clan"]},"color":"grey","id":"warrior-nomad","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/warriornomad.svg"},"1015":{"Name":"Flat Matt","virtual":true,"flat":true,"color":"white","id":"flat-matt","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/flatmatt.svg"},"1020":{"Name":"Water Mystery","physical":true,"mystery":true,"scatter":true,"color":"blue","id":"water-mystery","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/watermystery.svg"},"1057":{"Name":"Treasure Chest","evolution":"Reseller","evstage":3,"physical":true,"color":"brown","id":"treasure","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/treasurechest.svg"},"1086":{"Name":"Air Mystery","virtual":true,"mystery":true,"scatter":true,"color":"white","id":"air-mystery","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/airmystery.svg"},"1087":{"Name":"Feather","virtual":true,"scattered":true,"color":"white","id":"feather","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/feather.svg"},"1088":{"Name":"Golden Feather","virtual":true,"scattered":true,"color":"yellow","id":"golden-feather","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/goldenfeather.svg"},"1100":{"Name":"Pegasus","myth":true,"land":true,"bouncing":{"time":12,"on":[3,80,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,307,536,537,538,539,540,541,542,543,544,545,546,547,548,549,1086]},"color":"yellow","id":"pegasus","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/pegasus.svg"},"1101":{"Name":"Pegasus Host","mythhost":true,"virtual":true,"color":"white","id":"pegasus-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/pegasushost.svg"},"1107":{"Name":"Premium Personal","social":true,"premium":true,"color":"yellow","id":"premium-personal","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/premiumpersonal.svg"},"1021":{"Name":"Water Droplet","land":true,"scattered":true,"color":"blue","id":"water-droplet","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/waterdroplet.svg"},"1137":{"Name":"Fall Seasonal","physical":true,"seasonal":true,"color":"brown","id":"fall-spring","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/fallseasonalmunzee.svg"},"1139":{"Name":"Spring Seasonal","physical":true,"seasonal":true,"color":"purple","id":"seasonal-spring","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/springseasonalmunzee.svg"},"1151":{"Name":"Gnome Leprechaun","myth":true,"alterna":true,"land":true,"bouncing":{"time":12,"on":[0,441,851]},"color":"red","id":"gnome-leprechaun","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/gnomeleprechaun.svg"},"1168":{"Name":"Cyclops","myth":true,"land":true,"bouncing":{"time":12,"on":[40,131,148,218,242,290,584,681]},"color":"grey","id":"cyclops","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/cyclops.svg"},"1169":{"Name":"Cyclops Physical Host","mythhost":true,"physical":true,"color":"green","id":"cyclops-physical-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/cyclopshost.svg"},"1170":{"Name":"Cyclops Virtual Host","mythhost":true,"virtual":true,"color":"white","id":"cyclops-virtual-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/cyclops_virtual_host.svg"},"1237":{"Name":"Cherub","myth":true,"alterna":true,"land":true,"bouncing":{"time":12,"on":["NEEDSWORK"]},"color":"pink","id":"cherub","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/cherub.svg"},"1238":{"Name":"Cherub Virtual Host","alternahost":true,"virtual":true,"color":"white","id":"cherub-virtual-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/cherub_virtual_host.svg"},"1240":{"Name":"Tuli","evolution":"Pouch","evstage":1,"pouch":true,"land":true,"bouncing":{"time":6,"on":[0,532]},"color":"orange","id":"tuli","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/tuli.svg"},"1241":{"Name":"Tulimber","evolution":"Pouch","evstage":2,"pouch":true,"land":true,"bouncing":{"time":6,"on":[0,532]},"color":"orange","id":"tulimber","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/tulimber.svg"},"1243":{"Name":"Tuli Host","pouchhost":true,"physical":true,"color":"green","id":"tuli-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/firepouchcreaturehost.svg"},"1245":{"Name":"Temporary Virtual","virtual":true,"temporary":true,"color":"white","id":"temp-virtual","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/temporaryvirtual.svg"},"1246":{"Name":"Briefcase","zeecret":true,"scatter":true,"physical":true,"color":"black","id":"briefcase","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/briefcase.svg"},"1247":{"Name":"Dossier","scattered":true,"land":true,"zeecret":true,"color":"yellow","id":"dossier","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/dossier.svg"},"1248":{"Name":"Catapult","clan":true,"virtual":true,"scatter":true,"color":"brown","id":"catapult","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/catapult.svg"},"1255":{"Name":"Boulder","land":true,"clan":true,"scattered":true,"color":"grey","id":"boulder","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/boulder.svg"},"1268":{"Name":"Ogre","myth":true,"alterna":true,"land":true,"bouncing":{"time":12,"on":["NEEDSWORK"]},"color":"red","id":"ogre","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/ogre.svg"},"1269":{"Name":"Ogre Host","alternahost":true,"physical":true,"color":"green","id":"ogre-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/ogrehost.svg"},"1271":{"Name":"Chinese Dog","zodiac":true,"czodiac":true,"physical":true,"color":"yellow","id":"china-dog","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/dogchinesezodiac.svg"},"1272":{"Name":"Chinese Pig","zodiac":true,"czodiac":true,"physical":true,"color":"pink","id":"china-pig","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/pigchinesezodiac.svg"},"1338":{"Name":"Flat Lou","flat":true,"virtual":true,"color":"white","id":"flat-lou","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/flatlou.svg"},"1339":{"Name":"POI Historic","poi":true,"virtual":true,"color":"orange","id":"poi-historic","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/poihistoricalplace.svg"},"1340":{"Name":"POI Library","poi":true,"virtual":true,"color":"orange","id":"poi-library","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/poilibrary.svg"},"1341":{"Name":"POI First Responders","poi":true,"virtual":true,"color":"orange","id":"poi-fr","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/poifirstresponders.svg"},"1342":{"Name":"POI Faith","poi":true,"virtual":true,"color":"orange","id":"poi-faith","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/poifaithplace.svg"},"1351":{"Name":"Towel Day 2018","temporary":true,"land":true,"bouncing":{"time":12,"on":[0]},"color":"green","id":"towel-day-2018","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/towelday2018.svg"},"1352":{"Name":"Hitch Hiker 2018","temporary":true,"land":true,"scattered":true,"pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/hitchhiker2018.svg","color":"green","id":"hitch-hiker-2018"},"1370":{"Name":"Vesi","land":true,"bouncing":{"time":6,"on":[0,1200]},"pouch":true,"id":"vesi","evolution":"Pouch","evstage":1,"color":"lightblue","pin":"https://munzee.global.ssl.fastly.net/images/pins/vesi.png"},"1373":{"Name":"Water Pouch Creature Host","pouchhost":true,"physical":true,"color":"blue","id":"water-pouch-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/waterpoucecreaturehost.svg"},"1374":{"Name":"Cobra Yoga Pose","temporary":true,"land":true,"bouncing":{"time":6,"on":[0]},"color":"orange","id":"cobra-yoga-2018","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/cobrayogapose.svg"},"1376":{"Name":"Lotus Yoga Pose","temporary":true,"land":true,"bouncing":{"time":6,"on":[0]},"color":"orange","id":"lotus-yoga-2018","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/lotusyogapose.svg"},"1377":{"Name":"Warrior II Yoga Pose","temporary":true,"land":true,"bouncing":{"time":6,"on":[0]},"color":"orange","id":"warriorii-yoga-2018","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/warrioriiyogapose.svg"},"1378":{"Name":"Mermaid","myth":true,"bouncing":{"time":12,"on":["NEEDSWORK"]},"land":true,"color":"green","id":"mermaid","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mermaid.svg"},"1379":{"Name":"Mermaid Host","mythhost":true,"physical":true,"color":"blue","id":"mermaid-host","pin":"https://munzee.global.ssl.fastly.net/images/pins/svg/mermaidhost.svg"}};
	}
	// console.log(munzees);
    async function live(un){return (await db.access.find({username: un})).length > 0;}
	var names = {
		cap_p: "Cap", 
		dep_p: "Dep", 
		con_p: "CapOn",
		tot_p: "Total",
		cap: "#Cap",
		gam_cap: "#GamCap",
		phy_dep: "#PhyDep",
		myth_cap: "#MythCap",
		phy_cap: "#PhyCap",
		c_days: "#CapDays",
		d_days: "#DepDays",
		jewel_dep: "#JewelDep",
		clan_dep: "#WeaponDep"
	}

	var livelist = ['cap_p','dep_p','con_p','tot_p','c_days','d_days'];
	
    var conv = {
		cap_p: ['d','capture_points','capture'],
		dep_p: ['d','deploy_points','deploy'],
		con_p: ['d','capture_on_points','capture_on'],
		tot_p: ['d','total_points','total'], 
		cap: ['s','number of captures'], 
		gam_cap: ['s','number of gaming captures'], 
		phy_dep: ['s','number of physical deploys'], 
		dep: ['s','number of deploys'], 
		myth_cap: ['s','number of myth captures'],
		phy_cap: ['s','number of physical captures'],
		c_days: ['s','number of capture days'],
		d_days: ['s','number of deploy days'],
		jewel_dep: ['s','number of jewel deploys'],
		clan_dep: ['s','number of weapon deploys']
	}
	var timenow = hqT()
	var clan = [],token = await getAT(),requ = requirements[timenow.getMonth()],reqls = [],allreq = ['cap_p','dep_p','con_p','tot_p'];
	if(requirements[timenow.getMonth() + 1] && clanid == -1){
		requ = requirements[timenow.getMonth() + 1];
	}
	Object.keys(requ.user[4]).map((item)=>{reqls.push({t: "user",i: item})});
	Object.keys(requ.clan[4]).map((item)=>{reqls.push({t: "clan",i: item})});
	reqls.map((item)=>{if(!allreq.includes(item.i))allreq.push(item.i)});
	var table = [];
	if(clanid == 0 || clanid == -1){
		if(clanid == -1 && !requirements[timenow.getMonth() + 1]) {
			console.log('NMNA')
			return 'next-month-not-available';
		}
		console.log(reqls, allreq);
		table[0] = [];
		table[0][0] = {b: "Lvl"};
		for(var x = 0;x < reqls.length;x++){
			let b = reqls[x].t == "user" ? "User " : "Clan ";
			let c = names[reqls[x].i];
			table[0][x+1] = {b: b + c};
		}
		for(var i = 0;i < 5;i++){
			table[i+1] = [];
			table[i+1][0] = {b: (i + 1),l: i};
			for(var j = 0;j < reqls.length;j++){
				table[i+1][j+1] = {b: requ[reqls[j].t][i][reqls[j].i] || 0,l: i};
			}
		}
		var monthlist = ['January','February','March','April','May','June','July','August','September','October','November','December'];
		return {name: clanid == -1 ? "Next Month's Requirements" : "Requirements",tagline: monthlist[clanid == -1 ? timenow.getMonth()+1 : timenow.getMonth()] + " " + timenow.getFullYear(), logo: "/img/mace2.png", members: 5, ranking: `0`, id: clanid,
		table: table, level: "Infinity!"};
	}
	clan[0] = await mR('clan/',{"clan_id":clanid},token);
	clan[1] = await mR('clan/stats',{"clan_id":clanid},token);
	if(clan[1] && (logclan || clanid == 1)) {
		logclan = false;
		console.log('Clan Data', clan[1]);
	} else if(logclan || clanid == 1) {
		console.log('Noclandata');
	}

	if(timenow.getDate() < 3){
		if(timenow.getDate() > 1) console.log('NEW MONTH STATS',clan[1]);
		return {name: clan[0].details.name,tagline: clan[0].details.tagline, logo: clan[0].details.logo, members: clan[0].users.length, ranking: `No Active Clan War`, id: clanid,
	table: [[{l: -2, b: "No Active Clan War"}]], level: 0};
	}
	if(!clan[0]) return;
	
	// MAIN \/
	var dayactivity = {};
	for(var j = 0;j < clan[0].users.length;j++){
		var user = clan[0].users[j];
		table[j + 1] = [{b:user.username}];
		for(var i = 0;i < allreq.length;i++){
			var req = allreq[i];
			var cv = conv[req];
			if(cv[0] == 'd'){
				table[j+1][i+1] = {b: user[cv[1]] || 0};
			} else {
				if(!clan[1]){
					table[j+1][i+1] = {b: 0};
				} else {
					if(!clan[1][cv[1]]) clan[1][cv[1]] = {};
					table[j+1][i+1] = {b: clan[1][cv[1]][user.username] || 0};
				}
			}
		}
		// LIVE \/

		var month,date,today,udata;
		today = hqT();
		month = String(today.getMonth() + 1);
		date = String(today.getDate());
		if(await live(user.username)){
			table[j+1][0].live = true;
			if(month.length == 1){
				month = "0" + month;
			}
			if(date.length == 1){
				date = "0" + date;
			}
			udata = await mR('statzee/player/day',{"day":`${today.getFullYear()}-${month}-${date}`},await getAT(user.username));
			
			var depday = false;
			var capday = false;
			
			for(var k = 0;k < udata.captures.length;k++){
				//Captures
				if(allreq.includes('cap')) rows[j][allreq.indexOf('cap')+1] += 1;
				cap = Object.assign({}, udata.captures[k]);


				var munzz = munzees[Number(cap["capture_type_id"].toString().replace(/[^0-9]/g,'')).toString()];
				if(!munzz){
					for(var cc = 0;cc < Object.keys(munzees).length;cc++){
						// console.log(Object.keys(munzees)[cc].toString(), cap["capture_type_id"].toString(), cap["capture_type_id"])
						if(Object.keys(munzees)[cc].toString() == cap["capture_type_id"].toString()){
							munzz = Object.assign({}, munzees[Object.keys(munzees)[cc]]);
							console.log('Bettr Fix for', Object.keys(munzees)[cc], munzz);
						}
					}
				}
				
				// for(var cc = 0;cc < Object.keys(munzees).length;cc++){
				// 	// console.log(Object.keys(munzees)[cc].toString(), cap["capture_type_id"].toString(), cap["capture_type_id"])
				// 	if(Object.keys(munzees)[cc].toString() == cap["capture_type_id"].toString()){
				// 		console.log('UBER ERROR', Object.keys(munzees)[cc]);
				// 		munzz = munzees[Object.keys(munzees)[cc]];
				// 	}
				// }
				if(!munzz){
					// This is to let you know that MOBlox' Munzee List still needs more work, and what can be done
					let munz = await mR('munzee/',{url:`/m/${cap.username}/${cap.code}`},token);
					munz.closest = null;

					if(!captureids.includes(cap["capture_type_id"])){
						var rand = Math.floor(Math.random()*1000);
						for(var i = 0;i < `${JSON.stringify(munz)}\n\n${JSON.stringify(Object.assign({}, udata.captures[k]))}`.length / 1800;i++){
							rp({
								method: 'POST',
								uri: config.munzee_list_hook,
								formData: {
									username: 'NO MUNZEE DATA FOR CAPTURE',
									content: `CAPTURE RND: ${rand}` + "\`\`\`" + `${JSON.stringify(munz)}\n\n${JSON.stringify(Object.assign({}, udata.captures[k]))}`.substr(i*1800,1800) + "\`\`\`"
								}
							})
								.then(console.log)
								.catch(console.log);
						}
						captureids.push(cap["capture_type_id"]);
					} else {
						// console.log(cap.capture_type_id, captureids);
					}
					// console.log('NO MUNZEE DATA FOR CAPTURE',JSON.stringify(munz), JSON.stringify(cap)); 
					
				} else {

					// May 18
					if(munzz.gaming && allreq.includes('gam_cap')) table[j+1][allreq.indexOf('gam_cap')+1].b += 1;

					// June 18
					if(munzz.physical && allreq.includes('phy_cap')) table[j+1][allreq.indexOf('phy_cap')+1].b += 1;
					if((munzz.mythhost || munzz.alternahost || munzz.pouchhost) && allreq.includes('phy_cap')) table[j+1][allreq.indexOf('phy_cap')+1].b += 1;
					if(munzz.myth && allreq.includes('myth_cap')) table[j+1][allreq.indexOf('myth_cap')+1].b += 1;

					// July 18
					if(!munzz.social) capday = true;
				}
			}

			for(var k = 0;k < udata.deploys.length;k++){
				//DEPLOYS
				if(allreq.includes('dep')) rows[j][allreq.indexOf('dep')+1] += 1;
				dep = Object.assign({}, udata.deploys[k]);


				var munzz = Object.assign({}, munzees[Number(dep["capture_type_id"].toString().replace(/[^0-9]/g,'')).toString()]);
				
				if(!munzz){
					for(var cc = 0;cc < Object.keys(munzees).length;cc++){
						// console.log(Object.keys(munzees)[cc].toString(), cap["capture_type_id"].toString(), cap["capture_type_id"])
						if(Object.keys(munzees)[cc].toString() == dep["capture_type_id"].toString()){
							munzz = Object.assign({}, munzees[Object.keys(munzees)[cc]]);
							console.log('Bettr Fix for', Object.keys(munzees)[cc], munzz);
						}
					}
				}
				if(!munzz){
					// This is to let you know that MOBlox' Munzee List still needs more work, and what can be done
					let munz = await mR('munzee/',{url:`/m/${clan.users[j].username}/${dep.code}`},token);
					munz.closest = null;

					if(!captureids.includes(dep["capture_type_id"])){
						var rand = Math.floor(Math.random()*1000);
						for(var i = 0;i < `${JSON.stringify(munz)}\n\n${JSON.stringify(Object.assign({}, udata.deploys[k]))}`.length / 1800;i++){
							rp({method: 'POST',uri: config.munzee_list_hook,formData: {username: 'NO MUNZEE DATA FOR DEPLOY',content: `DEPLOY RND: ${rand}` + "\`\`\`" + `${JSON.stringify(munz)}\n\n${JSON.stringify(Object.assign({}, udata.deploys[k]))}`.substr(i*1800,1800) + "\`\`\`"}})
								.then(console.log)
								.catch(console.log);
						}
						captureids.push(dep["capture_type_id"]);
					} else {
						console.log(dep.capture_type_id, captureids);
					}
					
					// console.log('NO MUNZEE DATA FOR DEPLOY', JSON.stringify(munz), JSON.stringify(dep)); 
				} else {

					// June 18
					if(munzz.physical && allreq.includes('phy_dep')) table[j+1][allreq.indexOf('phy_dep')+1].b += 1;

					// July 18
					if(munzz.clan && allreq.includes('clan_dep')) table[j+1][allreq.indexOf('clan_dep')+1].b += 1;
					if(munzz.jewel && allreq.includes('jewel_dep')) table[j+1][allreq.indexOf('jewel_dep')+1].b += 1;
					depday = true;
				}
			}
		}

		// Non-live Cap and Deploy Days
		capday = false;
		depday = false;
		if(!clan[1] || !clan[1]["capture"]){
			if(user.capture_points > 0){
				capday = true;
			}
		} else {
			if(user.capture_points > (clan[1]["capture"][user.username] || 0)){
				capday = true;
			}
		}

		if(!clan[1] || !clan[1]["deploy"]){
			if(user.deploy_points > 0){
				depday = true;
			}
		} else {
			if(user.deploy_points > (clan[1]["deploy"][user.username] || 0)){
				depday = true;
			}
		}
		// END

		if(depday && allreq.includes('d_days')) table[j+1][allreq.indexOf('d_days')+1].b += 1;
		if(capday && allreq.includes('c_days')) table[j+1][allreq.indexOf('c_days')+1].b += 1;
		dayactivity[user.username] = [capday, depday];

		// LIVE /\
	}
	table[0] = [{b: 'Player'}];
	var ct = clan[0].users.length + 1
	table[ct] = [{b: 'Clan Total'}];
	for(var i = 0;i < allreq.length;i++){ //Clan Total and top bar
		table[0][i + 1] = {b: names[allreq[i]], live: livelist.includes(allreq[i])};
		table[ct][i+1] = {b:0};
		if(allreq[i] == "c_days" || allreq[i] == "d_days"){
			table[ct][i+1].b = 300;
		}
		for(var j = 0;j < ct - 1;j++){
			if(allreq[i] != "c_days" && allreq[i] != "d_days"){
				table[ct][i+1].b += table[j+1][i+1].b;
			} else {
				table[ct][i+1].b = Math.min(table[ct][i+1].b, table[j+1][i+1].b);
			}
		}
	}
	table[0][table[0].length] = {b: "Lvl"};
	table[ct][0].l = 4;
	var clanlevel = 4;
	for(var i = 1;i < table.length;i++){ // Colouring
		var user = table[i][0];
		user.l = 4;
		for(var j = 1;j < table[i].length;j++){
			var cell = table[i][j];
			var cu; // Clan or User
			cu = i < ct ? "user" :"clan";
			cell.l = 4;
			for(var k = 4;k >= 0;k--){
				let req = requ[cu][k][allreq[j - 1]];
				// console.log(j-1, req, cell.b);
				if(req != undefined && req > cell.b){
					cell.l -= 1;
					user.l = Math.min(user.l,cell.l)
				}
			}
		}
		clanlevel = Math.min(clanlevel, user.l);
		table[i][table[i].length] = {b: table[i][0].l + 1,l: table[i][0].l};
	}
	table[ct][0].l = clanlevel;
	table[ct][table[ct].length - 1] = {b: table[ct][0].l + 1,l: table[ct][0].l};

	// MAIN /\

	return {name: clan[0].details.name,tagline: clan[0].details.tagline, logo: clan[0].details.logo, members: clan[0].users.length, ranking: `${clan[0].ranking} / ${clan[0].number_of_clans}`, id: clanid,
	table: table, level: table[ct][0].l + 1, day: dayactivity};
}

async function getUserData(userid){
	return [[{l: 1,b: 'soon'}]];
}

app.all('/',async function(req, res){
	page = fs.readFileSync('page.html','utf8');
	if(!page) page = fs.readFileSync('page.html','utf8');
	var zstats = await db.stats.findOne({});
	if(!zstats) zstats = {up: 1, pop: 1};
	res.send(page.replace('$$$$STATS%%%%',`<b>Stats</b>: ${(await db.cl.find({})).length} Clans Added! ${zstats.up} Unique Users! ${(await db.access.find({})).length} Authenticated Users!`));
});
app.all('/stats', async function(req, res){
	var x = await db.stats.findOne({});
	var sending = `<style>pre {white-space: pre-wrap; word-wrap: break-word;}</style>CLANS:<br><pre><code>${JSON.stringify(await db.cl.find({}))}</code></pre><br><br>USERS:<br><pre><code>${JSON.stringify((await db.access.find({})).map(user=>user.username))}</code></pre><br><br>People online right now: ${x.pop}<br><br>Total (non-unique) people ever: ${x.tpe}<br><br>"Unique" People: ${x.up}<br><br><br>Data since ~14:15 on 21/06/18`;

	res.send(sending);
});
app.all('/list', async function(req, res){
	res.send(JSON.parse(await rp('https://gitlab.com/MOBlox/munzee-list/raw/master/munzee-list.json')));
})
app.all('/fr',async function(req, res){
	page = fs.readFileSync('fr.html','utf8');
	if(!page) page = fs.readFileSync('fr.html','utf8');
	res.send(page);
});
app.all('/pirate',async function(req, res){
	page = fs.readFileSync('page.html','utf8');
	for(var key in translations){
		var regex = new RegExp("([^a-zA-Z0-9])" + key + "([^a-zA-Z0-9])", 'gi');
		page = page.replace(regex, function(match, a, b){
			if(match.toLowerCase() == match){
				return (a + translations[key][2] + b).toLowerCase();
			} else {
				return a + translations[key][2] + b;
			}
		});
	}
	res.send(page);
});
app.all('/single', async function(req, res){
	if(!req.query.clan){
		res.send('No clan');
	} else {
		var e = {id: Number(req.query.clan)}
		if(!clanlist[e.id] || e.force){
			clanlist[e.id] = await getClanData(e.id);
			if((await db.cl.find({id: e.id})).length == 0){
				db.cl.insert({id: e.id});
			}
		}
		if(clanlist[e.id] != 'next-month-not-available') {
			var clan = clanlist[e.id];
		} else {
			return res.send('Next month\'s requirements not available');
		}
		var send = `
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
		<style>
		body {
			padding: 20px;
		}
		table {
            border: 1px solid black;
            border-bottom: 1px solid black;
        }
        table td,
        table th {
            text-align: center;
            font-size: 12px;
            padding: 2px;
            margin: 0;
            border-radius: 0;
        }
		td[lvl="-1"],
        tr[lvl="-1"] {
            background-color: #FF0000;
        }
        
        td[lvl="0"],
        tr[lvl="0"] {
            background-color: #EF6500;
        }
        
        td[lvl="1"],
        tr[lvl="1"] {
            background-color: #FA9102;
        }
        
        td[lvl="2"],
        tr[lvl="2"] {
            background-color: #FCD302;
        }
        
        td[lvl="3"],
        tr[lvl="3"] {
            background-color: #BFE913;
        }
        
        td[lvl="4"],
        tr[lvl="4"] {
            background-color: #55F40B;

        }
		</style>
			<h5 class="center" style="margin: 0;"><img style="height: 1em;width: 1em;" src="${clan.logo}">${clan.name}</h5>
			<h6 class="center" style="margin: 0; margin-bottom: 10px;">${clan.tagline}</h6>
			<table style="width: 100%;">`

		for(var i = 0;i < clan.table.length;i++){
			let row = clan.table[i];
			send += `<tr>`;
			for(var j = 0;j < row.length;j++){
				let item = row[j];
				send += `<td class="${j % 2 == 0 && i == 0 ? 'grey lighten-2 ' : ''}" lvl="${item.l}">`
				if(item.live){
					send += `<b v-if="item.live">${item.b}</b>`
				} else {
					if(clan.ranking == 0 && item.b == 0){
						send += `<b>---</b>`
					} else {
						send += item.b
					}
				}		
				send += `</td>`;
			}
			send += `</tr>`;
		}

		send += `</table>`
		res.send(send);
	}
})

app.all('/handleoauth',async function(req, res){
	if(req.query.code) {
		console.log(req.query.code);
		var options = {
			method: 'POST',
			uri: 'https://api.munzee.com/oauth/login',
			url: 'https://api.munzee.com/oauth/login',
			formData: {
				'client_id': '05ee5f19369e31d7ae38357525a18b53',
				'client_secret': 'f75c7336cfb9c6c3826f2aea',
				'grant_type': 'authorization_code',
				'code': req.query.code, 
				'redirect_uri': 'https://zee.moblox.co.uk/handleoauth'
			}
		};
		rp(options)
			.then(async function (body) {
				var data = JSON.parse(body).data;
				console.log(data);
				data.username = await getUName(data.user_id, data);
				if(await getAT(data.user_id)){
					await db.access.update({user_id: data.user_id}, data);
				} else {
					await db.access.insert([data]);
				}
				rp({
					method: 'POST',
					uri: config.auth_hook,
					formData: {
						username: 'Player Authentication Stats',
						content: `Player Authenticated: ${data.username}`
					}
				})
					.then(console.log)
					.catch(console.log);
				//
				res.send(`<script>window.location.replace('https://zee.moblox.co.uk/');</script>`);
			})
			.catch(async function (err) {
				res.send(`${err} - Error - Redirecting in 3 seconds<script>setTimeout(()=>{window.location.replace('https://zee.moblox.co.uk/')},3000);</script>`);
			});
	} else {
		res.send(`<script>window.location.replace('https://zee.moblox.co.uk/');</script>`);
	}
})

app.all('/c',async function(req, res){
	res.send(`
	<script>
		var clans = [0,-1,1349,457,1441];
		console.log(clans);
		var clancookie = [];
		if(document.cookie.includes && document.cookie.includes("clans=")){
			var cookiez = document.cookie.split('; ');
			var cookies = cookiez.map(function(item){
				if(item.split('=')[0] == "clans"){
					clancookie = JSON.parse(item.split('=')[1]);
				}
				return item.split('=');                
			});
			localStorage.clans = clans;
			document.cookie = 'clans=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		}
		if(!localStorage.clans) localStorage.clans = "[0]";
		clancookie = JSON.parse(localStorage.clans);
		for(var i = 0;i < clans.length;i++){
			var clan = clans[i];
			if(clancookie.indexOf(clan) < 0){
				clancookie.push(clan);
			}
		}
		localStorage.clans = JSON.stringify(clancookie);
		// document.cookie = "clans=" + JSON.stringify(clancookie) + "; expires=Thu, 31 Dec " + ((new Date(Date.now())).getFullYear() + 1) + " 12:00:00 UTC";
		window.location.replace('https://zee.moblox.co.uk/');
	</script>
	`);
	// res.send(`
	// <script>
	// 	if(!localStorage.clans) localStorage.clans = "[0,1349,457,1441]";
	// 	// if(!document.cookie.includes('clans=')) {
	// 	// 	document.cookie = "clans=[0,1349,457,1441]; expires=Thu, 31 Dec " + ((new Date(Date.now())).getFullYear() + 1) + " 12:00:00 UTC";
	// 	// }
	// 	window.location.replace('https://zee.moblox.co.uk');
	// 	setTimeout(function(){window.location.replace('https://zee.moblox.co.uk')},5000);
	// </script>
	// <h1>We have a new clan page! If this is your first time, you'll be redirected in 5 seconds</h1>
	// <h4 onclick="window.location.replace('https://zee.moblox.co.uk');">You can also click here to go right there</h4>
	// <h4>The new clan page allows you to add other clans and will have more features in the near future!</h4>
	// `);
});

app.all('/addclans',async function(req, res){
	if(!req.query.clans){
		res.send(`<script>window.location.replace('https://zee.moblox.co.uk/');</script>`);
	} else {
		res.send(`<script>
			var clans = ${req.query.clans};
			console.log(clans);
			var clancookie = [];
            if(document.cookie.includes("clans=")){
                var cookiez = document.cookie.split('; ');
                var cookies = cookiez.map(function(item){
                    if(item.split('=')[0] == "clans"){
                        clancookie = JSON.parse(item.split('=')[1]);
                    }
                    return item.split('=');                
                });
                localStorage.clans = clans;
                document.cookie = 'clans=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
			}
			if(!localStorage.clans) localStorage.clans = "[0]";
			clancookie = JSON.parse(localStorage.clans);
			for(var i = 0;i < clans.length;i++){
				var clan = clans[i];
				if(clancookie.indexOf(clan) < 0){
					clancookie.push(clan);
				}
			}
			localStorage.clans = JSON.stringify(clancookie);
			// document.cookie = "clans=" + JSON.stringify(clancookie) + "; expires=Thu, 31 Dec " + ((new Date(Date.now())).getFullYear() + 1) + " 12:00:00 UTC";
			window.location.replace('https://zee.moblox.co.uk/');
		</script>`);
	}
});

/*
French Translations:
Requirement: Besoin
Requirements: Besoins
Authenticate: Authentifier
Create clan shortcut: Créer un raccourci de clan
Munzee is a registered trademark of Freeze Tag Inc.: Munzee est une marque déposée de Freeze Tag Inc.
Player: Joueur
Level: Niveau
Ranking: Classement
Members: Membres
For this month: Pour ce mois
Add a Clan: Ajouter un clan
Add: Ajouter
*/

app.all('/reload',async function(req, res){
	page = null;
	res.send('<script>window.location.replace(\'/\'');
});

var server = http.createServer(app);
var io = socketio.listen(server);

async function reloadClan(id){
	clanlist[id] = await getClanData(id);
	io.emit('clan',clanlist[id]);
}

var stats;
// var peopleonpage = 0; // pop
// var totalpeopleever = 0; // tpe
// var uniquepeople = 0; // up
// var uniqueips = []; // uip
io.on('connection', async function(socket){
	//console.log(socket.handshake.headers);
	if(!stats){
		var statt = await db.stats.findOne({});
		statt.pop = 0;
		await db.stats.update({},statt);
	}
	stats = await db.stats.findOne({});
	if(!stats) {
		stats = {};
		await db.stats.insert({});
	}
	stats.pop = stats.pop + 1 || 1;
	stats.tpe = stats.tpe + 1 || 1;
	// peopleonpage++;
	// totalpeopleever++;
	if(!stats.uip) stats.uip = [];
	if(!stats.uip.includes(socket.handshake.headers["cf-connecting-ip"])){
		stats.uip.push(socket.handshake.headers["cf-connecting-ip"])
		stats.up = stats.up + 1 || 1;
		// uniqueips.push(socket.handshake.headers["cf-connecting-ip"]);
		// uniquepeople++;
	}
	rp({
		method: 'POST',
		uri: config.people_hook,
		formData: {
			username: 'People on Page Stats',
			content: `Person Connected (${socket.handshake.headers["cf-connecting-ip"]}, ${socket.handshake.headers["cf-ipcountry"]})\n${stats.pop} people on CuppaZee!\n${stats.tpe} Total People!\n${stats.up} Unique People! Data since 14:15 21/06/18`
		}
	})
		.then(console.log)
		.catch(console.log);
	await db.stats.update({},stats);
	socket.on('disconnect', async function(e){
		stats = await db.stats.findOne({});
		stats.pop--;
		// peopleonpage--;
		rp({
			method: 'POST',
			uri: config.people_hook,
			formData: {
				username: 'People on Page Stats',
				content: `Person Disconnected (${socket.handshake.headers["cf-connecting-ip"]}, ${socket.handshake.headers["cf-ipcountry"]})\n${stats.pop} people on CuppaZee!\n${stats.tpe} Total People!\n${stats.up} Unique People! Data since 14:15 21/06/18`
			}
		})
			.then(console.log)
			.catch(console.log);
		await db.stats.update({},stats);
	});
	socket.on('admin-toast', async function(e){
		if(e.pw == "x2d5gK") io.emit('toast', e.m);
	});
	socket.on('requestclan',async function(e){
		if(!clanlist[e.id] || e.force){
			clanlist[e.id] = await getClanData(e.id);
			if((await db.cl.find({id: e.id})).length == 0){
				db.cl.insert({id: e.id});
			}
		}
		if(clanlist[e.id] != 'next-month-not-available') {
			io.emit('clan',clanlist[e.id]);
		} else {
			console.log('next-month-not-available');
		}
	});
	socket.on('requestuser',async function(e){
		if(!userlist[e.id] || e.force){
			userlist[e.id] = await getUserData(e.id);
		}
		io.emit('user',{id: e.id, info: userlist[e.id]});
	});
	socket.on('requestrequirements',async function(e){
		io.emit('requirements',requirements[(new Date(Date.now())).getMonth()]);
	});
	socket.on('getclanid',async function(e){
		rp({
			method: 'POST',
			uri: config.clan_hook,
			formData: {
				username: 'Clan Added',
				content: `Clan Added\n${e}`
			}
		})
			.then(console.log)
			.catch(console.log);
		if(/^\d+$/.test(e.replace('https','http').replace('http://stats.munzee.dk/?',''))) {
			if((await mR('clan/',{"clan_id":Number(e.replace('https','http').replace('http://stats.munzee.dk/?',''))})).users.length > 0) {
				return socket.emit('addclan',Number(e.replace('https','http').replace('http://stats.munzee.dk/?','')))
			}
		}
		var x = (await mR('clan/id/',{simple_name: e.replace('http://www.munzee.com/clans/','').replace('https://www.munzee.com/clans/','').replace('http://munzee.com/clans/','').replace('https://munzee.com/clans/','').replace(/[^A-z0-9]/g,'').toLowerCase()})).clan_id;
		if(x != 0){
			socket.emit('addclan',x);
		} else {
			var clanpage = (await rp('https://www.munzee.com/clans')).toLowerCase().replace(/[^a-z0-9<>\\\/]/g,'');
			var regexStatement = new RegExp('<ahref/clans/([^/]+)/>' + e.replace('http://www.munzee.com/clans/','').replace('https://www.munzee.com/clans/','').replace('http://munzee.com/clans/','').replace('https://munzee.com/clans/','').replace(/[^A-z0-9]/g,'').toLowerCase() + '</a>')
			var matching = clanpage.match(regexStatement);
			if(matching){
				x = (await mR('clan/id/',{simple_name: matching[1]})).clan_id;
				socket.emit('addclan',x);
			} else {
				var clanpage = (await rp('https://www.munzee.com/clans')).toLowerCase().replace(/[^a-z0-9<>\\\/]/g,'');
				var regexStatement = new RegExp('<ahref/clans/([^/]+)/>the' + e.replace('http://www.munzee.com/clans/','').replace('https://www.munzee.com/clans/','').replace('http://munzee.com/clans/','').replace('https://munzee.com/clans/','').replace(/[^A-z0-9]/g,'').toLowerCase() + '</a>')
				var matching = clanpage.match(regexStatement);
				if(matching){
					x = (await mR('clan/id/',{simple_name: matching[1]})).clan_id;
					socket.emit('addclan',x);
				} else {
					console.log('CLAN NOT AVAILABLE', e);
					socket.emit('addclan',0);
				}
			}
		}
	});
	socket.on('getshot',async function(i){
		// var stream = webshot('https://zee.moblox.co.uk/single?clan=' + i);
		// stream.on('data', function(data){
		// 	socket.emit('shot','data:image/png;base64,' + data.toString('base64'));
		// })
		/* var stream = webshot('https://zee.moblox.co.uk/single?clan=0');stream.on('data', function(data){console.log('shot','data:image/png;base64,' + data.toString('base64'));}) */
		//data:text/plain;base64,SGVsbG8sIFdvcmxkIQ%3D%3D
		/*
		
	(async() => {
		const browser = await puppeteer.launch({args: [
			'--no-sandbox',
			'--disable-setuid-sandbox'
		]})
		const page = await browser.newPage()
		await page.goto('https://zee.moblox.co.uk/single?clan=0')
		const screenshot = await page.screenshot()
		console.log(screenshot.toString('base64'))
		browser.close()
	})()

		*/
	});
});

var thru = 0;
var loop = setInterval(async function(){
	var list = await db.cl.find({});
	var start = Math.floor((list.length / 15) * (thru % 15));
	var end = ((list.length / 15) * ((thru % 15) + 1)) - 1;
	// console.log(thru, list, start, end);
	for(var i = start;i <= end;i++){
		reloadClan(list[i].id);
	}
	thru++;
	if(thru > 100 && thru % 15 == 0){
		thru = 0;
	}
	logclan = true;
	timezone = JSON.parse(await rp('https://api.timezonedb.com/v2/get-time-zone?key=S0T3SKWNPU73&format=json&by=position&lat=33.214561&lng=-96.614456'));
},60000);

server.listen(config.port || process.env.PORT || 8080).on('error', function(err) { console.log(err) });
console.log('Listening');

