**__This Project is no longer in development. Please see [CuppaZee v2](https://gitlab.com/MOBlox/CuppaZee-2). Thank you.__**

**CuppaZee**

CuppaZee (Formerly BloxZee) was originally just a Clan Stats page for "The Cup of" [Tea, Coffee and Cocoa] Clans.

I decided to rewrite the project from scratch, and make it fully public, with lots of stats and information. 

**Credits**

Munzee.DK / RUJA.DK for inspiration for this project, and for their amazing clan stats (Which has loads of features that I don't have) at https://stats.munzee.dk

TheGenie18 for the suggestion to make this project